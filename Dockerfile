FROM gcr.io/deeplearning-platform-release/base-cu101
RUN conda update --all &&\
    conda install mono jupyterhub jupyterlab libsndfile cmake zeromq caffe theano pip && \
    pip install -U --no-cache-dir setuptools wheel dotnetcore2 git+https://github.com/pythonnet/pythonnet cntkx modin[all] pycuda onnxruntime-gpu horovod nvidia-pyindex hummingbird-ml h2o nimbusml fastai ffmpeg chaostoolkit chaostoolkit-google-cloud-platform && \
    conda clean --all -f -y